import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Yumicell extends StatelessWidget {
  const Yumicell({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget yumicell1 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 1',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell2 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 2',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell3 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 3',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell4 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 4',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell5 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 5',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell6 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 6',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget yumicell7 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 7',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    Widget text1 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     ยูมี ก็เหมือนกับหญิงสาวทั่วไป เธอมีชีวิตที่ปกติในทุกๆ วัน ครองสถานะโสดมาสักพักนึง โฟกัสกับชีวิตการงานที่ดูมีกำลังใจขึ้นหน่อย ในขณะเดียวกัน ในโลกอยู่ใบหนึ่งคือเซลล์สมองของยูมีก็มีอีกกองเอาใจช่วยเธออยู่ ไม่ว่าจะเป็น เซลล์ความรัก, เซลล์ความหิว หรือ เซลล์ความชอบธรรม'
              ' ทุกเซลล์ลุ้นและช่วยบาลานซ์ชีวิตให้กับยูมีอยู่ใกล้ชิด ไม่ว่าเธอจะคิดหรือตัดสินใจทำอะไร ก็มีพวกเขาคอยเป็นกำลังใจ แต่ดูเหมือนว่ารุ่นน้องที่ทำงาน อย่าง อูกี จะกลายเป็นชาเลนจ์ใหม่ให้กับหัวใจของเธอ ก็นิดนึง...เด็กมันน่ารักดี',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text2 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     และเซลล์ที่ยูมีเก็บซ่อนเอาไว้ในสถานที่ลึกสุด...ก็ถูกเรียกกลับมาให้ปรากฏตัวขึ้นอีกครั้ง ยูมีตื่นเต้นสุดๆ กับการได้ไปงานเทศกาลดอกไม้กับอูกี แบบสองต่อสอง'
              ' ที่ไม่ต่างกับการออกเดตอีกครั้งในรอบ 3 ปีเลย อากาศดี บรรยากาศก็ดูเหมือนจะเป็นใจ แต่ทว่า...ชายหนุ่มมาดเซอร์ บุคลิกแปลกๆ'
              ' แต่งตัวไม่ดูสถานที่ อย่าง กูอุง ได้ปรากฏตัวขึ้นต่อหน้ายูมีกลางคัน หรือนี่จะเป็นสัญญาณที่จะแฝงความหมายอะไรกันแน่นะ?',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
         Widget text3 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     แม้ว่าการนัดบอดเจอกันเป็นครั้งแรกของ ยูมี กับ กูอุง ออกจะตะขิดตะขวงใจไปสักหน่อย แต่พอได้ลองออกเดตด้วยกันแค่ไม่กี่ชั่วโมง...ก็พบว่าเป็นการเริ่มต้นที่ไม่แย่'
              ' แต่อย่างไรก็ตาม ข่าวการนัดบอดของยูมีได้แพร่สะพัดไปทั่วออฟฟิศอย่างรวดเร็ว และในขณะเดียวกันนั้น'
              ' เซลล์ภายในร่างกายของยูมี กับ กูอุง ต่างกันทำหน้าที่ของตัวเองอย่างหนัก พยายามและไตร่ตรองอย่างเข้มข้น เพื่อพิจารณาว่า...ควรจะยื่นมือคว้าความสัมพันธ์นี้เอาไว้ดีหรือไม่นะ?',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text4 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     "ฉัน...มีแฟนแล้วเหรอคะ?" ภายหลังจากที่ ยูมี หมดสติลงไปอย่างกะทันหัน กูอุง ก็รีบพาเธอไปโรงพยาบาล แต่เธอตื่นขึ้นด้วยการมึนงง'
              ' และยิ่งตกใจเข้าไปใหญ่ที่พยาบาลบอกว่า "แฟนพามาส่ง" ทำให้บัดนี้ เซลล์ความรักของยูมีได้รับมามีบทบาทอีกครั้ง'
              ' เช่นเดียวกับ กูอุงที่ดูเหมือนจะแฮปปี้กับความสัมพันธ์ครั้งนี้ไม่น้อย และเซลล์ของทั้งคู่กำลังผสานร่วมกันไปตามภาวะอารมณ์ปัจจุบันที่เกิดขึ้น',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text5 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     "งั้นไปที่บ้านฉันไหม?" ความสัมพันธ์ระหว่าง ยูมี กับ กูอุง ค่อยๆ พัฒนาคืบหน้าไปอย่างเป็นลำดับขั้น'
              ' แม้ว่าทั้งคู่จะอยู่ในช่วงที่เขาเรียกกันว่า "ศึกษาดูใจ" แต่ความรู้สึกและหัวใจของยูมีก็กระชุ่มกระชวยขึ้นมาไม่น้อย'
              ' และช่วงเวลาที่ทั้งคู่จะได้ใช้ร่วมกันสองต่อสองก็เกิดขึ้น จนกระทั่ง...มาสะดุดตรงที่ แชอี เพื่อนผู้หญิงของกูอุง ที่มาทำให้เธอเกิดอาการลังเลใจ',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text6 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     "แค่คิดก็ฟินแล้ววว..!" กูอุง ออกปากชวน ยูมี ไปทริปเที่ยวต่างจังหวัดด้วยกันเป็นครั้งแรก โดยวางแผนไปเที่ยวพักผ่อนเป็นเวลา 3 วัน 2 คืน แบบสองต่อสอง'
              ' งานนี้ต่อมความจิ้นและมโนของบรรดาเซลล์ต่างๆ ในร่างกายของยูมีก็อดคิดเพ้อล่วงหน้าไปก่อนไม่ได้'
              ' โดยเฉพาะเจ้าเซลล์ลามกตัวดีที่คิดไปไกลสักหน่อย การเตรียมตัวไปทริปครั้งนี้ของทั้งคู่ดูจะลงตัวดี กระทั่งถึงวันดีเดย์ที่ดูเหมือนว่าจะมีเรื่อง...',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text7 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     ยินดีต้อนรับเข้าสู่...วังวนคนคลั่งรัก ดูเหมือนว่าความรักของ ยูมี กับ กูอุง กำลังไปได้สวยและลงตัวเลยทีเดียว'
              ' เขาหลงยูมีไปเต็มๆ หัวใจในตอนนี้ จนกระทั่งเกิดชาเลนจ์ใหม่ที่ท้าทาย'
              ' เมื่อเพื่อนของยูมีกำลังจะแต่งงาน โดยที่ข่าวว่ายูมีมีแฟนใหม่แล้วได้แพร่สะบัดไปทั้งกลุ่ม บททดสอบครั้งสำคัญที่เธอหวังจะให้กูอุงไปงานแต่งเพื่อนกับเธอ เพื่อเป็นการเปิดตัวอย่างเป็นทางการ...ขึ้นได้เริ่มต้นขึ้นอย่างท้าทาย',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'Yumi Cell',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      body: ListView(children: [
        Image.asset(
          'images/yumi.jpg',
          width: 250,
          height: 400,
          fit: BoxFit.cover,
        ),
        yumicell1,
        text1,
        yumicell2,
        text2,
        yumicell3,
        text3,
        yumicell4,
        text4,
        yumicell5,
        text5,
        yumicell6,
        text6,
        yumicell7,
        text7,
      ]),
    );
  }
}
