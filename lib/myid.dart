import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Myid extends StatelessWidget {
  const Myid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget myid1 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 1',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    Widget text1 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     คังมีแร สาวเกาหลีที่ถูกสังคมตัดสินว่าเป็น “ยัยอัปลักษณ์” ไม่เคยได้เข้ากลุ่มกับเพื่อนสักครั้ง มีความรักก็ไม่สมหวัง อีกทั้งยังโดนรังควาญ ความเจ็บช้ำน้ำใจจากคนเหล่านั้นก็สั่งสมเป็นเงินก้อนโตพาเธอไปศัลยกรรมตบแต่งใบหน้าเสียใหม่'
              ' เปิดเทอมเข้ามหาวิทยาลัยมา คังมีแร เฟรชชี่ปี 1 ก็ได้พบกับสังคมใหม่ ที่ใดมีที่พักใจก็ย่อมมีมารผจญ ทั้ง ๆ ที่ทำศัลยกรรมมาจนเช้งวับแถมยังหัดแต่งหน้าซะเป๊ะปัง'
              ' ก็ยังมิวายเจอคนหน้าดี จิตใจทรามเข้ามาระรานแบบอ้อม ๆ ในร้ายมีดี คังมีแร ได้เจอ โดคยองซอก เพื่อนสมัย ม.ปลาย ที่มีอดีตเชื่อมโยงกันอยู่ทำให้ชีวิตในรั้วมหาวิทยาลัยของเธอมีสีสันขึ้นทันควัน',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'My ID Is Gangnam Beauty',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      body: ListView(children: [
        Image.asset(
          'images/myid.jpg',
          width: 250,
          height: 400,
          fit: BoxFit.cover,
        ),
        myid1,
        text1,
      ]),
    );
  }
}
