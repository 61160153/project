import 'package:flutter/material.dart';
import 'package:project/castle.dart';
import 'package:project/here.dart';
import 'package:project/itaewon.dart';
import 'package:project/lighonme.dart';
import 'package:project/main.dart';
import 'package:project/mother.dart';
import 'package:project/myid.dart';
import 'package:project/painter.dart';
import 'package:project/turebeauty.dart';
import 'package:project/yumicell.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget textSection = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'การ์ตูนฮิตประจำสัปดาห์',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    Widget iconHome = Container(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => Turebeauty()),
                    // );
                  },
                  child: Text(
                    'เว็บคอมมิค',
                    softWrap: true,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ])),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => Turebeauty()),
                    // );
                  },
                  child: Text(
                    'อีคอมมิค',
                    softWrap: true,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ])),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => Turebeauty()),
                    // );
                  },
                  child: Text(
                    'เว็บโนเวล',
                    softWrap: true,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ])),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => Turebeauty()),
                    // );
                  },
                  child: Text(
                    'อีโนเวล',
                    softWrap: true,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ])),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.black),
                  ),
                  onPressed: () {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => Turebeauty()),
                    // );
                  },
                  child: Text(
                    'ชาลเลนจ์',
                    softWrap: true,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ])),
        ],
      ),
    );

    Widget icon = Container(
      padding: EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const <Widget>[
          Icon(
            Icons.facebook,
            color: Colors.black,
            size: 36.0,
          ),
          Icon(
            Icons.email,
            color: Colors.black,
            size: 36.0,
          ),
          Icon(
            Icons.call,
            color: Colors.black,
            size: 36.0,
          ),
          Icon(
            Icons.map,
            color: Colors.black,
            size: 36.0,
          ),
        ],
      ),
    );

    Widget text = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ใหม่ล่าสุด',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    Widget next = Container(
      padding: EdgeInsets.all(20),
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          // ignore: deprecated_member_use
          RaisedButton(
              onPressed: () {},
              color: Colors.black,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Text(
                'Next',
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
    Widget search = Container(
      child: Container(
      width: double.infinity,
      height: 40,
      color: Colors.white,
      child: Center(
        child: TextField(
          decoration: InputDecoration(
            hintText: 'Search',
            prefixIcon: Icon(Icons.search),
          ),
        ),
      ),
      ),
    );

    Widget title = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
              child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/turebeauty.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Turebeauty()),
                  );
                },
                child: Text('true beauty'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/Itaewon Class.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Itaewon()),
                  );
                },
                child: Text('Itaewon Class'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/yumi.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Yumicell()),
                  );
                },
                child: Text('Yumi Cell'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/lighonme.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Lighonme()),
                  );
                },
                child: Text('Ligh on me'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/myid.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Myid()),
                  );
                },
                child: Text('My ID Is Gangnam Beauty'),
              )
            ],
          )),
        ],
      ),
    );

    Widget titleNew = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
              child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/mother.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Mother()),
                  );
                },
                child: Text('คุณแม่วัยใส'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/here.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Here()),
                  );
                },
                child: Text('Here U Are'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/castle.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Castle()),
                  );
                },
                child: Text('Castle'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/yumi.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Yumicell()),
                  );
                },
                child: Text('Yumi Cell'),
              )
            ],
          )),
          Expanded(
              child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  bottom: 5,
                ),
                child: Image.asset('images/painter.jpg',
                    width: 220, height: 200, fit: BoxFit.cover),
              ),
              TextButton(
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.black),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Painter()),
                  );
                },
                child: Text('Painter Of The Night'),
              )
            ],
          )),
        ],
      ),
    );

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Colors.black,
            title: const Text(
              'U CARTOON',
              style: TextStyle(
                fontSize: 50,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              search,
              Image.asset('images/webtoon.png',
                  width: 600, height: 240, fit: BoxFit.cover),
              iconHome,
              textSection,
              title,
              text,
              titleNew,
              titleNew,
              next,
              icon,
            ]),
          ),
        ],
      ),
    );
  }
}
