import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Castle extends StatelessWidget {
  const Castle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget castle1 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 1',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );

    Widget text1 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     ความแค้น...ต้องชำระด้วยเลือด” "คิมชิน" เป็นนักฆ่าฝีมือฉกาจที่เสียพ่อไปด้วยฝีมือของ "คาสเซิล" องค์กรอาชญากรรมระดับชาติของเกาหลี หลังจากที่คิมชินหลบภัยอยู่ที่รัสเซียนานถึง 8 ปี'
              ' เขาตัดสินใจกลับเกาหลีเพื่อแก้แค้น แต่แค่กระบวนการยุติธรรมไม่สามารถเอาชนะองค์กรอาชญากรรมที่มีอำนาจมากอย่างคาสเซิลได้'
              ' วิธีชนะมันมีเพียงวิธีเดียว...คือการรวบรวมคนที่แค้นพวกเขาก่อตัวเป็นปราสาทที่ใหญ่โตกว่าให้ได้!',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'Castle',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      body: ListView(children: [
        Image.asset(
          'images/castle.jpg',
          width: 250,
          height: 400,
          fit: BoxFit.cover,
        ),
        castle1,
        text1,
      ]),
    );
  }
}
