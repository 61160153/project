import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Painter extends StatelessWidget {
  const Painter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget painter1 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 1',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget painter2 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 2',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    Widget painter3 = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        'ตอนที่ 3',
        softWrap: true,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
    

    Widget text1 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     เรื่องราวเกิดขึ้นในยุคเกาหลีโบราณ พระเอกเป็นคุณชายบ้าตัณหา ชื่อเสียงเลื่องลือเรื่องกามราคะ เสพสมกับผู้ชายด้วยกัน'
              ' วันหนึ่งเกิดไปถูกใจภาพวาดลามกที่นายเอกเป็นคนวาด แล้วอยากให้เค้ามาวาดรูปตอนตัวเองกำลังเริงรักบ้าน'
              ' แต่ดันติดตรงนายเอกวางมือแล้ว แต่พระเอกเป็นคนอยากได้อะไรก็ต้องได้ ลากตัวเค้ามาอยู่ในจวน'
              ' บีบบังคับให้วาดรูป พอจับจุดได้ว่านายเอกแอบชอบอาจารย์ก็เอามาขู่ จนในที่สุดนายเอกก็ยินยอม',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
        Widget text2 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     นายเอกที่แม้จะวาดรูปโลกีย์แต่ความจริงยังบริสุทธิ์ผุดผ่อง ต้องมานั่งดูพระเอกโรมรันกับคู่ขาทุกค่ำคืน เพื่อจดจำไปวาดรูป'
              ' แล้วมันคงจะไม่มีปัญหาอะไร ถ้าคู่ขา (ที่คิดเกินเลยกับพระเอก) จะไม่จับสังเกตได้ว่า พระนายเค้าสบตากันอยู่ตลอด ความหึงหวงริษยานำพาให้มากลั่นแกล้งนายเอก'
              ' ทำลายภาพวาดของเค้า พระเอกก็คิดว่านายเอกบ่ายเบี่ยงไม่ยอมวาด สั่งให้ลากไปโบยแต่สุดท้ายก็ยกเลิกคำสั่ง'
              ' ในความโหดร้ายไร้หัวใจซึ่งถือเป็นความปกติของพระเอก แต่กับนายเอกมันต่างออกไป'
              ' ถ้ามองอย่างฉาบฉวยก็คงจะไม่ต่างจากคนอื่น แต่ถ้าสังเกตให้ดี ๆ จะรู้เลยว่านายเอกได้รับความเอ็นดูมาก',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
         Widget text3 = Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            Text(
              '     ครั้งแรกที่มีอะไรกันเกิดจากนายเอกเมามายไร้สติ คิดว่าตัวเองได้ร่วมรักกับอาจารย์ที่แอบรัก แต่พอมีครั้งแรกก็มีครั้งต่อ ๆ ไป'
              ' นายเอกก็เสพติดเซ็กซ์ที่พระเอกมองให้ พระเอกก็ไม่เบื่อหน่ายที่จะกอดนายเอกซ้ำไปซ้ำมา …',
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 18),
            ),
          ],
        ));
       

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'Painter of the night',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      body: ListView(children: [
        Image.asset(
          'images/painter.jpg',
          width: 250,
          height: 400,
          fit: BoxFit.cover,
        ),
        painter1,
        text1,
        painter2,
        text2,
        painter3,
        text3,
        
      ]),
    );
  }
}
